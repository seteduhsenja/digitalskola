


function fungsi1a() {
  document.getElementById("bgroup1").style.display = "block";
  document.getElementById("judul1").style.display = "none";
  document.getElementById("bgroup2").style.display = "none";
  document.getElementById("bgroup3").style.display = "none";
  document.getElementById("bgroup4").style.display = "none";

  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul3").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}


function fungsi1b() {
  document.getElementById("bgroup1").style.display = "none";
  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul3").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}


function fungsi2a() {
  document.getElementById("bgroup2").style.display = "block";
  document.getElementById("judul2").style.display = "none";

  document.getElementById("bgroup1").style.display = "none";
  document.getElementById("bgroup3").style.display = "none";
  document.getElementById("bgroup4").style.display = "none";

  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul3").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}


function fungsi2b() {
  document.getElementById("bgroup2").style.display = "none";

  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul3").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}

function fungsi3a() {
  document.getElementById("bgroup3").style.display = "block";
  document.getElementById("judul3").style.display = "none";

  document.getElementById("bgroup1").style.display = "none";
  document.getElementById("bgroup2").style.display = "none";
  document.getElementById("bgroup4").style.display = "none";

  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}

function fungsi3b() {
  document.getElementById("bgroup3").style.display = "none";
  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul3").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}

function fungsi4a() {
  document.getElementById("bgroup4").style.display = "block";
  document.getElementById("judul4").style.display = "none";

  document.getElementById("bgroup1").style.display = "none";
  document.getElementById("bgroup2").style.display = "none";
  document.getElementById("bgroup3").style.display = "none";

  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul3").style.display = "block";
}

function fungsi4b() {
  document.getElementById("bgroup4").style.display = "none";

  document.getElementById("judul1").style.display = "block";
  document.getElementById("judul2").style.display = "block";
  document.getElementById("judul3").style.display = "block";
  document.getElementById("judul4").style.display = "block";
}


// ===================== script FAQ Desktop ==================

function faq1() {
  document.getElementById("faq1").style.display = "none";
  document.getElementById("faq1a").style.display = "block";
  document.getElementById("faq1b").style.display = "block";

  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";

  document.getElementById("faq2a").style.display = "none";
  document.getElementById("faq2b").style.display = "none";
  document.getElementById("faq3a").style.display = "none";
  document.getElementById("faq3b").style.display = "none";
  document.getElementById("faq4a").style.display = "none";
  document.getElementById("faq4b").style.display = "none";
  document.getElementById("faq5a").style.display = "none";
  document.getElementById("faq5b").style.display = "none";
  document.getElementById("faq6a").style.display = "none";
  document.getElementById("faq6b").style.display = "none";
}

function faq1a() {
  document.getElementById("faq1a").style.display = "none";
  document.getElementById("faq1b").style.display = "none";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";
}

function faq2() {
  document.getElementById("faq2").style.display = "none";
  document.getElementById("faq2a").style.display = "block";
  document.getElementById("faq2b").style.display = "block";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";

  document.getElementById("faq1a").style.display = "none";
  document.getElementById("faq1b").style.display = "none";
  document.getElementById("faq3a").style.display = "none";
  document.getElementById("faq3b").style.display = "none";
  document.getElementById("faq4a").style.display = "none";
  document.getElementById("faq4b").style.display = "none";
  document.getElementById("faq5a").style.display = "none";
  document.getElementById("faq5b").style.display = "none";
  document.getElementById("faq6a").style.display = "none";
  document.getElementById("faq6b").style.display = "none";
}

function faq2a() {
  document.getElementById("faq2a").style.display = "none";
  document.getElementById("faq2b").style.display = "none";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";
}

function faq3() {
  document.getElementById("faq3").style.display = "none";
  document.getElementById("faq3a").style.display = "block";
  document.getElementById("faq3b").style.display = "block";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";

  document.getElementById("faq1a").style.display = "none";
  document.getElementById("faq1b").style.display = "none";
  document.getElementById("faq2a").style.display = "none";
  document.getElementById("faq2b").style.display = "none";
  document.getElementById("faq4a").style.display = "none";
  document.getElementById("faq4b").style.display = "none";
  document.getElementById("faq5a").style.display = "none";
  document.getElementById("faq5b").style.display = "none";
  document.getElementById("faq6a").style.display = "none";
  document.getElementById("faq6b").style.display = "none";
}

function faq3a() {
  document.getElementById("faq3a").style.display = "none";
  document.getElementById("faq3b").style.display = "none";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";
}

function faq4() {
  document.getElementById("faq4").style.display = "none";
  document.getElementById("faq4a").style.display = "block";
  document.getElementById("faq4b").style.display = "block";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";

  document.getElementById("faq1a").style.display = "none";
  document.getElementById("faq1b").style.display = "none";
  document.getElementById("faq2a").style.display = "none";
  document.getElementById("faq2b").style.display = "none";
  document.getElementById("faq3a").style.display = "none";
  document.getElementById("faq3b").style.display = "none";
  document.getElementById("faq5a").style.display = "none";
  document.getElementById("faq5b").style.display = "none";
  document.getElementById("faq6a").style.display = "none";
  document.getElementById("faq6b").style.display = "none";
}

function faq4a() {
  document.getElementById("faq4a").style.display = "none";
  document.getElementById("faq4b").style.display = "none";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";
}


function faq5() {
  document.getElementById("faq5").style.display = "none";
  document.getElementById("faq5a").style.display = "block";
  document.getElementById("faq5b").style.display = "block";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq6").style.display = "block";

  document.getElementById("faq1a").style.display = "none";
  document.getElementById("faq1b").style.display = "none";
  document.getElementById("faq2a").style.display = "none";
  document.getElementById("faq2b").style.display = "none";
  document.getElementById("faq3a").style.display = "none";
  document.getElementById("faq3b").style.display = "none";
  document.getElementById("faq4a").style.display = "none";
  document.getElementById("faq4b").style.display = "none";
  document.getElementById("faq6a").style.display = "none";
  document.getElementById("faq6b").style.display = "none";
}

function faq5a() {
  document.getElementById("faq5a").style.display = "none";
  document.getElementById("faq5b").style.display = "none";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";
}

function faq6() {
  document.getElementById("faq6").style.display = "none";
  document.getElementById("faq6a").style.display = "block";
  document.getElementById("faq6b").style.display = "block";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";

  document.getElementById("faq1a").style.display = "none";
  document.getElementById("faq1b").style.display = "none";
  document.getElementById("faq2a").style.display = "none";
  document.getElementById("faq2b").style.display = "none";
  document.getElementById("faq3a").style.display = "none";
  document.getElementById("faq3b").style.display = "none";
  document.getElementById("faq4a").style.display = "none";
  document.getElementById("faq4b").style.display = "none";
  document.getElementById("faq5a").style.display = "none";
  document.getElementById("faq5b").style.display = "none";
}

function faq6a() {
  document.getElementById("faq6a").style.display = "none";
  document.getElementById("faq6b").style.display = "none";

  document.getElementById("faq1").style.display = "block";
  document.getElementById("faq2").style.display = "block";
  document.getElementById("faq3").style.display = "block";
  document.getElementById("faq4").style.display = "block";
  document.getElementById("faq5").style.display = "block";
  document.getElementById("faq6").style.display = "block";
}


// ===================== script FAQ Mobile ==================

function faq1m() {
  document.getElementById("faq1m").style.display = "none";
  document.getElementById("faq1am").style.display = "block";
  document.getElementById("faq1bm").style.display = "block";

  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";

  document.getElementById("faq2am").style.display = "none";
  document.getElementById("faq2bm").style.display = "none";
  document.getElementById("faq3am").style.display = "none";
  document.getElementById("faq3bm").style.display = "none";
  document.getElementById("faq4am").style.display = "none";
  document.getElementById("faq4bm").style.display = "none";
  document.getElementById("faq5am").style.display = "none";
  document.getElementById("faq5bm").style.display = "none";
  document.getElementById("faq6am").style.display = "none";
  document.getElementById("faq6bm").style.display = "none";
}

function faq1am() {
  document.getElementById("faq1am").style.display = "none";
  document.getElementById("faq1bm").style.display = "none";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";
}

function faq2m() {
  document.getElementById("faq2m").style.display = "none";
  document.getElementById("faq2am").style.display = "block";
  document.getElementById("faq2bm").style.display = "block";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";

  document.getElementById("faq1am").style.display = "none";
  document.getElementById("faq1bm").style.display = "none";
  document.getElementById("faq3am").style.display = "none";
  document.getElementById("faq3bm").style.display = "none";
  document.getElementById("faq4am").style.display = "none";
  document.getElementById("faq4bm").style.display = "none";
  document.getElementById("faq5am").style.display = "none";
  document.getElementById("faq5bm").style.display = "none";
  document.getElementById("faq6am").style.display = "none";
  document.getElementById("faq6bm").style.display = "none";
}

function faq2am() {
  document.getElementById("faq2am").style.display = "none";
  document.getElementById("faq2bm").style.display = "none";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";
}

function faq3m() {
  document.getElementById("faq3m").style.display = "none";
  document.getElementById("faq3am").style.display = "block";
  document.getElementById("faq3bm").style.display = "block";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";

  document.getElementById("faq1am").style.display = "none";
  document.getElementById("faq1bm").style.display = "none";
  document.getElementById("faq2am").style.display = "none";
  document.getElementById("faq2bm").style.display = "none";
  document.getElementById("faq4am").style.display = "none";
  document.getElementById("faq4bm").style.display = "none";
  document.getElementById("faq5am").style.display = "none";
  document.getElementById("faq5bm").style.display = "none";
  document.getElementById("faq6am").style.display = "none";
  document.getElementById("faq6bm").style.display = "none";
}

function faq3am() {
  document.getElementById("faq3am").style.display = "none";
  document.getElementById("faq3bm").style.display = "none";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";
}

function faq4m() {
  document.getElementById("faq4m").style.display = "none";
  document.getElementById("faq4am").style.display = "block";
  document.getElementById("faq4bm").style.display = "block";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";

  document.getElementById("faq1am").style.display = "none";
  document.getElementById("faq1bm").style.display = "none";
  document.getElementById("faq2am").style.display = "none";
  document.getElementById("faq2bm").style.display = "none";
  document.getElementById("faq3am").style.display = "none";
  document.getElementById("faq3bm").style.display = "none";
  document.getElementById("faq5am").style.display = "none";
  document.getElementById("faq5bm").style.display = "none";
  document.getElementById("faq6am").style.display = "none";
  document.getElementById("faq6bm").style.display = "none";
}

function faq4am() {
  document.getElementById("faq4am").style.display = "none";
  document.getElementById("faq4bm").style.display = "none";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";
}


function faq5m() {
  document.getElementById("faq5m").style.display = "none";
  document.getElementById("faq5am").style.display = "block";
  document.getElementById("faq5bm").style.display = "block";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";

  document.getElementById("faq1am").style.display = "none";
  document.getElementById("faq1bm").style.display = "none";
  document.getElementById("faq2am").style.display = "none";
  document.getElementById("faq2bm").style.display = "none";
  document.getElementById("faq3am").style.display = "none";
  document.getElementById("faq3bm").style.display = "none";
  document.getElementById("faq4am").style.display = "none";
  document.getElementById("faq4bm").style.display = "none";
  document.getElementById("faq6am").style.display = "none";
  document.getElementById("faq6bm").style.display = "none";
}

function faq5am() {
  document.getElementById("faq5am").style.display = "none";
  document.getElementById("faq5bm").style.display = "none";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";
}

function faq6m() {
  document.getElementById("faq6m").style.display = "none";
  document.getElementById("faq6am").style.display = "block";
  document.getElementById("faq6bm").style.display = "block";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";

  document.getElementById("faq1am").style.display = "none";
  document.getElementById("faq1bm").style.display = "none";
  document.getElementById("faq2am").style.display = "none";
  document.getElementById("faq2bm").style.display = "none";
  document.getElementById("faq3am").style.display = "none";
  document.getElementById("faq3bm").style.display = "none";
  document.getElementById("faq4am").style.display = "none";
  document.getElementById("faq4bm").style.display = "none";
  document.getElementById("faq5am").style.display = "none";
  document.getElementById("faq5bm").style.display = "none";
}

function faq6am() {
  document.getElementById("faq6am").style.display = "none";
  document.getElementById("faq6bm").style.display = "none";

  document.getElementById("faq1m").style.display = "block";
  document.getElementById("faq2m").style.display = "block";
  document.getElementById("faq3m").style.display = "block";
  document.getElementById("faq4m").style.display = "block";
  document.getElementById("faq5m").style.display = "block";
  document.getElementById("faq6m").style.display = "block";
}
