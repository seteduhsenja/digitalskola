{% set engineer = [
		{
			image: 'assets/img/tutor-de/abdul_gifari.png',
			name: 'Ali Firdaus Ghifari',
			desc: 'Data Engineering <br/>Bukalapak',
			linkedin: 'https://www.linkedin.com/in/afghifari'
		},{
			image: 'assets/img/tutor-de/dimasta.png',
			name: 'Dimasta Juniar',
			desc: 'Data Engineering <br/>Bukalapak',
			linkedin: 'https://www.linkedin.com/in/dimasta-juniar-652122105'
		},{
			image: 'assets/img/tutor-de/rio.png',
			name: 'Rio',
			desc: 'Big Data and Engineering<br/>LinkAja',
			linkedin: 'https://www.linkedin.com/in/harapanr'
		},{
			image: 'assets/img/tutor-de/rizki_dermawan.png',
			name: 'Rizki Dermawan',
			desc: 'Data Engineering <br/>Bukalapak',
			linkedin: 'https://www.linkedin.com/in/rizkidermawan'
		}
	]

%}