{# Data yang dipakai di beberapa page #}
{% set kelas = {
	science: {
		image: 'assets/icon/sc_ds.jpg',
		title: 'Data Science Batch - 7',
		text: 'Belajar Data Science tanpa latar belakang IT. Kelas intensif selama tiga bulan (42 sesi) dengan metode online interactive.',
		status: 'Open',
		url: 'skola-class-data-science.html'
	},
	marketing: {
		image: 'assets/icon/sc_dm.jpg',
		title: 'Digital Marketing - Batch 5',
		text: 'Kuasai Digital Marketing hanya dalam tiga bulan. 35 sesi belajar dengan metode online interactive bersama tutor-tutor profesional.',
		status: 'Open',
		url: 'skola-class-digital-marketing.html'
	},
	ui_ux:{
		image: 'assets/img/sc_ui.jpg',
		title: 'UI/UX - Batch 1',
		text: 'Kelas intensif UI/UX design selama 3 bulan (35 sesi). Belajar hardskill & softskill dari 0 sampai siap kerja langsung dari ahlinya.',
		status: 'Open',
		url: 'skola-class-ui-ux.html'
	},
	engineer:{
		image: 'assets/img/sc_de.jpg',
		title: 'Data Engineer - Batch 1',
		text: 'Belajar dari 0 hingga siap kerja sebagai Data Engineer. 45 sesi belajar selama 4 bulan dengan metode belajar dan kurikulum terbaik.',
		status: 'Open',
		url: 'skola-class-data-engineer.html'
	}

} %}

{% set expert = {
	excel: {
		image: 'assets/img/SE-Excel-t.jpg',
		title: 'SkolaExpert Excel - Batch 2',
		subtitle: 'Learn Data Analysis with Excel',
		text: 'Kuasai pengolahan dan analisa data dengan Microsoft Excel hanya dalam 4 hari',
		status: 'Closed',
		url: 'skola-expert-excel.html'
	},
	story_telling: {
		image: 'assets/img/SE-Tableau-t.jpg',
		title: 'SkolaExpert Data Storytelling',
		subtitle: '...',
		text: '.....',
		status: 'Closed',
		url: 'skola-expert-tableau.html'
	},
	tableau: {
		image: 'assets/img/SE-Tableau-t.jpg',
		title: 'SkolaExpert Data Storytelling',
		subtitle: 'Fast and Easy Data Visualization with Tableau',
		text: 'Kuasai Visualisasi Data dengan Tableau hanya dalam 3 hari',
		status: 'Open',
		url: 'skola-expert-tableau.html'
	}
} %}

{% set testimony = [{
		image: 'assets/testimoni/rifyal.png',
		name: 'Rifyal Tumber',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Science',
		testi: 'Kelas yang diikuti sejauh ini cukup menarik dan banyak ilmu yang didapat. Tutornya adalah praktisi di bidangnya, jadi kita dapat banyak wawasan mengenai pekerjaannya sebagai Data Scientist.'
	},{
		image: 'assets/testimoni/dania.png',
		name: 'Dania Alfis Firdausyah',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Science',
		testi: 'Aku seneng sih belajar di Digital Skola. Materinya disusun dengan sangat baik, mentor dan host nya juga sangat profesional dan sangat mengayomi.'
	},{
		image: 'assets/testimoni/sri.png',
		name: 'Sri Artha Ningsih Togatorop',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Science',
		testi: 'Masih week 2, so far menyenangkan dan tutor dan team Digital Skola sangat responsif dan antusias. kita yang belajar juga jadi antusias.'
	},{
		image: 'assets/testimoni/rahul.png',
		name: 'Rahul Muliana',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Science',
		testi: 'Tujuan awal belajar di sini salah satunya pingin tahu real case dari seorang Data Scientist di perusahaan, mulai dari pola processing data sampai best practicenya. Materinya menarik, bisa nanya banyak dan konsultasi ke mentornya sehingga bisa melewati milestone learning path kita.'
	},{
		image: 'assets/testimoni/dm_andrie.png',
		name: 'Andrie Safargi',
		peserta: 'Peserta SkolaClass',
		kelas: 'Digital Marketing',
		testi: 'Latar belakang saya engineering tapi belajar digital marketing di Digital Skola. Tutor yang luar biasa membimbing saya dengan contoh-contoh study case sehingga saya cepat memahami. Terima kasih juga untuk Digital Skola yang memberikan terobosan-terobosan baru dalam melaksanakan training ini.'
	},{
		image: 'assets/testimoni/de_arinan.png',
		name: 'Arinan Najah Putra',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Engineering',
		testi: 'Karena materi nya based on industri, saya belajar sesuatu yang baru. Cara belajarnya juga learning by doing jadi lebih mudah mengerti, apalagi tutor nya juga sudah berpengalaman. Saya juga merasakan improvement cukup banyak, salah satunya saya jadi lebih tahu cara penggunaan tools buat data engineer.'
	},{
		image: 'assets/testimoni/ds_ricardo.png',
		name: 'Ricardo',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Science',
		testi: 'Saya sangat sering stuck belajar Data Science, tapi berkat mentor dan teman-teman yang suportif, masalah yang dihadapi bisa dibahas dan diselesaikan bersama. Selama belajar di DigitalSkola, saya belajar tidak hanya belajar coding, tapi juga softskill seperti presentasi, teamwork, bahkan tentang cara menghadapi interview. Terima kasih Digital Skola!'
	},{
		image: 'assets/testimoni/ds_wira.png',
		name: 'Wira',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Science',
		testi: 'Tim Digital Skola mulai dari CEO, para tutor dan class representative sangat ramah dan sangat suportif. Materi pembelajarannya juga terstruktur dan diberikan tugas/assignment yang sangat membantu untuk mencari tahu lebih hal-hal baru yang tidak diajarkan di kelas.'
	}
	]

%}