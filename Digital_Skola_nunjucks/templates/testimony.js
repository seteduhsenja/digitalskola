{% set engineer = [
	{
		image: 'assets/testimoni/de_arinan.png',
		name: 'Arinan Najah Putra',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Engineering',
		testi: 'Karena materi nya based on industri, saya belajar sesuatu yang baru. Cara belajarnya juga learning by doing jadi lebih mudah mengerti, apalagi tutor nya juga sudah berpengalaman. Saya juga merasakan improvement cukup banyak, salah satunya saya jadi lebih tahu cara penggunaan tools buat data engineer.'
	},{
		image: 'assets/testimoni/de_harryyanto.png',
		name: 'Harryyanto Ishaq Agasi',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Engineering',
		testi: 'Saya rasa karena pengajarnya sudah punya experience di dunia kerja, jadi lebih ada trust sama yang diajarkan. Kemudian, karena tersedia video rekaman, jadinya enak jika ingin mengulang pelajarannya.'
	},{
		image: 'assets/testimoni/de_dewi_p.png',
		name: 'Dewi Paramithasari',
		peserta: 'Peserta SkolaClass',
		kelas: 'Data Engineering',
		testi: 'Karena background aku murni bukan IT, awalnya aku mikir keras untuk ikut camp Data Engineering di Digital Skola. Ternyata setelah masuk, aku ngerasain banget bahwa tutor Digital Skola mau mengerti dan ngajarin pelan-pelan dan memastikan bahwa semuanya ngerti dan mau untuk di WA secara pribadi. Satu lagi yg aku suka dr tutornya enggak show off berlebihan perihal pekerjaannya.'
	}
	]
%}

{% set excel = [
	{
		image: 'assets/testimoni/se_rianti.png',
		name: 'Rianti',
		peserta: 'Peserta SkolaExpert',
		kelas: 'Python Programming',
		testi: 'Kebetulan aku gak ada basic IT dan so far aku ngerasa dapat pencerahan bangettt, karena diajarin dari basic banget. Kemarin selama 7 sesi itu super worth it sih, jadi tau dasar-dasarnya dari Phyton seperti apa, cukup simple, sederhana, dan mudah dimengerti.. Makasi ya DigitalSkola'
	},
	{
		image: 'assets/testimoni/se_gregorius_kevin.png',
		name: 'Gregorius Kevin',
		peserta: 'Peserta SkolaExpert',
		kelas: 'Python Programming',
		testi: 'Jadi menurut ku, belajar Skola Expert Programming itu seru banget, tutornya benar-benar memahami materi, bahkan dia ajarin dari dasar dan menggunakan tools yang menurut saya mudah untuk dipahami walaupun orang tersebut belum memiliki background IT, intinya sukses terus buat Digital Skola, semoga melahirkan banyak talenta baru, dan terus mencari tutor baru yang berkualitas yaa! Yukk Join di Digital Skolaa!'
	}
]
%}